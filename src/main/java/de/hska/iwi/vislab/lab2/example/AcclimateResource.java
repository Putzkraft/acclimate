package de.hska.iwi.vislab.lab2.example;

import java.util.ArrayList;
import java.util.List;

public class AcclimateResource {
    private static AcclimateResource instance;
    private List<DataReading> sensorData = new ArrayList<>();
    private Integer temperature = 0;

    public static AcclimateResource getInstance() {
        if (instance == null) {
            instance = new AcclimateResource();
        }
        return instance;
    }

    // Web interface (acclimate_mode) asks for
    public String getSensorMood() {
        String sensorMood;
        if (sensorData.isEmpty()) {
            sensorMood = "empty";
        } else {
            if (evaluateAverageSensorData()) {
                sensorMood = "happy";
            } else {
                sensorMood = "not happy";
            }
        }
        System.out.println("sensormood = " + sensorMood);
        return sensorMood;
    }

    public String getVotes() {
        //System.out.println("temperature = " + temperature);
        if (temperature == 0) {
            return "even";
        } else {
            return "not even";
        }
    }

    // POST request is sent from arduino
    public void postSensorData(String data) {
        String[] sensorDataComponents = data.split(",");
        try {
            float temp = Float.valueOf(sensorDataComponents[0]);
            int co2 = Integer.parseInt(sensorDataComponents[1]);
            int noise = Integer.parseInt(sensorDataComponents[2]);
            int photo = Integer.parseInt(sensorDataComponents[3]);
            DataReading newDataReading = new DataReading(temp, co2, noise, photo);
            addNewDataReadingToQueue(newDataReading);
            //System.out.println("Added new data reading: " + newDataReading);
        } catch (Exception e) {
            System.out.println("Unable to convert data to Float/Integer:" + data);
        }
    }

    private void addNewDataReadingToQueue(DataReading newDataReading) {
        if (sensorData.size() == 10) {
            sensorData.remove(0);
        }
        sensorData.add(newDataReading);
    }

    // POST request is sent from web interface
    public void postVotes(String vote) {
        System.out.println(vote);
        if (vote.equals("cold")) {
        //if (vote == "cold"){
            temperature -= 1;
        }
        if (vote.equals("warm")){
            temperature += 1;
        }
    }

    private float getAverageTemp() {
        int size = sensorData.size();
        float sum = 0;
        for (int i = 0; i < size; i++) {
            sum += sensorData.get(i).getTemperature();
        }
        return sum / size;
    }

    private float getAverageCO2() {
        int size = sensorData.size();
        float sum = 0;
        for (int i = 0; i < size; i++) {
            sum += sensorData.get(i).getCo2();
        }
        return sum / size;
    }
    private float getAverageNoise() {
        int size = sensorData.size();
        float sum = 0;
        for (int i = 0; i < size; i++) {
            sum += sensorData.get(i).getNoise();
        }
        return sum / size;
    }
    private float getAveragePhoto() {
        int size = sensorData.size();
        float sum = 0;
        for (int i = 0; i < size; i++) {
            sum += sensorData.get(i).getPhoto();
        }
        return sum / size;
    }

    // true = within bounds, false = off bounds
    private boolean evaluateAverageSensorData() {
        if (tempInBounds() && co2InBounds() && noiseInBounds() && photoInBounds()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean tempInBounds() {
        float t = getAverageTemp();
        //System.out.println("Average temperature: " + t);
        return (t > 19f && t < 24f);
    }

    private boolean co2InBounds() {
        float c = getAverageCO2();
        //System.out.println("Average CO2: " + c);
        return (c < 600f);
    }

    private boolean noiseInBounds() {
        float n = getAverageNoise();
        //System.out.println("Average noise: " + n);
        return (n < 520f);
    }

    private boolean photoInBounds() {
        float p = getAveragePhoto();
        //System.out.println("Average light: " + p);
        return (p > 100f && p < 250f);
    }
}


