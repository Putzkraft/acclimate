package de.hska.iwi.vislab.lab2.example;

import org.springframework.context.annotation.Bean;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Test in console with
 * curl -X POST -H "Content-Type: text/plain" --data "3.1,420" 192.168.178.48:8088/sensor-data
 * and replace the ip
 */
@CrossOrigin(origins = "*")
@Path("")
public class AcclimateService {
    AcclimateResource acclimateResource = AcclimateResource.getInstance();
/*
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        return "Hello Oauth!";
    }
    */
    @GET
    @Path("/sensor-data")
    @Produces(MediaType.TEXT_PLAIN)
    public String getSensorData() {
        return acclimateResource.getSensorMood();
    }

    @GET
    @Path("/vote")
    @Produces(MediaType.TEXT_PLAIN)
    public String getVotes() {
        return acclimateResource.getVotes();
    }

    @POST
    @Path("/sensor-data")
    @Consumes(MediaType.TEXT_PLAIN)
    public void postSensorData(String sensordata) {
        acclimateResource.postSensorData(sensordata);
    }

    @POST
    @Path("/vote")
    @Consumes(MediaType.TEXT_PLAIN)
    public void postVote(String vote) {
        acclimateResource.postVotes(vote);
    }
}