package de.hska.iwi.vislab.lab2.example;

public class DataReading {
    private float temperature;
    private int co2;
    private int noise;
    private int photo;

    public DataReading(float temperature, int co2, int noise, int photo) {
        this.temperature = temperature;
        this.co2 = co2;
        this.noise = noise;
        this.photo = photo;
    }

    public float getTemperature() {
        return temperature;
    }
    public int getCo2() { return co2; }
    public int getNoise() {
        return noise;
    }
    public int getPhoto() {
        return photo;
    }

    @Override
    public String toString() {
        return temperature + ", " + co2 + ", " + noise + ", " + photo;
    }
}
