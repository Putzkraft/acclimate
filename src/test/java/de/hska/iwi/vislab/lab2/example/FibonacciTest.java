package de.hska.iwi.vislab.lab2.example;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class FibonacciTest {

	private HttpServer server;
	private WebTarget target;

	@Before
	public void setUp() throws Exception {
		server = Main.startServer();
		Client c = ClientBuilder.newClient();
		target = c.target(Main.BASE_URI);
	}

	@After
	public void tearDown() throws Exception {
		server.shutdown();
	}

	@Test
	public void fibonacciTests() {
		assertFibonacciResponse(0);
		assertFibonacciResponse(0);
		assertFibonacciResponse(0);
		nextFibonacci();
		nextFibonacci();
		assertFibonacciResponse(1);
		nextFibonacci();
		assertFibonacciResponse(2);
		resetFibonacci();
		assertFibonacciResponse(0);
	}

	private void resetFibonacci() {
		target.path("fibonacci").request().accept(MediaType.TEXT_PLAIN).put(Entity.text(""));
	}

	private void nextFibonacci() {
		target.path("fibonacci").request().accept(MediaType.TEXT_PLAIN).post(Entity.text(""));
	}

	private void assertFibonacciResponse(int i) {
		String responseMsg;
		responseMsg = target.path("fibonacci").request().accept(MediaType.TEXT_PLAIN).get(String.class);
		System.out.println(responseMsg);
		assertEquals(i, Integer.parseInt(responseMsg));
	}
}
