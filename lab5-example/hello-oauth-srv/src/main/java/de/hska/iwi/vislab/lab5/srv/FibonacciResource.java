package de.hska.iwi.vislab.lab5.srv;

public class FibonacciResource {
    private int secondLast = 0;
    private int last = 1;
    private static FibonacciResource instance;

    public static FibonacciResource getInstance() {
        if (instance == null) {
            instance = new FibonacciResource();
        }
        return instance;
    }

    public int getSecondLast() {
        return this.secondLast;
    }

    public int getLast(){
        return this.last;
    }

    public void next() {
        int newFibonacci = secondLast + last;
        secondLast = last;
        last = newFibonacci;
    }

    public void reset() {
        secondLast = 0;
        last = 1;
    }
}
