package de.hska.iwi.vislab.lab5.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@CrossOrigin(origins = "*")
@Controller
public class HelloOauthWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloOauthWebApplication.class, args);
	}

	@RequestMapping("/")
	public String home() {
		return "mode_auswahl";
	}

	@Autowired
	private RestTemplate restTemplate;

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@RequestMapping("/acclimate_mode")
	public String acclimateMode() {
		return "acclimate_mode";
	}

	@GetMapping("/acclimate_mode/sensor-data")
	public String getSensordata(Model model) {
		String sensorRestTemplate = restTemplate.getForObject("http://localhost:8088/sensor-data", String.class);
		model.addAttribute("sd", sensorRestTemplate);
		return "acclimate_mode";
	}

	@GetMapping("/acclimate_mode/vote")
	public String getVotes(Model model) {
		String voteRestTemplate = restTemplate.getForObject("http://localhost:8088/vote", String.class);
		model.addAttribute("vote", voteRestTemplate);
		return "acclimate_mode";
	}

	@RequestMapping("/participant_mode")
	public String participantMode() {
		return "participant_mode";
	}

	@PostMapping("/participant_mode")
	public String tooCold() {
		System.out.println("Too cold...");

		return "participant_mode";
	}

}
