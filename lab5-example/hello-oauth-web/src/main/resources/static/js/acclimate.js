var sdHappy = true;
var votesEven = true;
var currentMoodHappy = true;


function start(){
    //document.getElementById("00").innerHTML =  "<img src=\"/images/begin.GIF\" width=\"300px\">";
    setInterval(GetData, 2000);
}

function PostVote(vote) {
    var url = "http://192.168.178.48:8088/vote";
    var method = "POST";
    var shouldBeAsync = true;
    var request = new XMLHttpRequest();
    request.onload = function() {
        var status = request.status;
        var data = request.responseText;
    }
    request.open(method, url, shouldBeAsync);
    request.setRequestHeader("Content-Type", "text/plain");
    request.send(vote);
}

// Gif updaten
function updateGif() {
    var nextMoodHappy;
    if (votesEven) {
        nextMoodHappy = sdHappy;
    } else {
        nextMoodHappy = false;
    }

    // Zustandsübergänge von good nach bad und zurück (siehe Zustandsübergangsdiagramm)
    if (currentMoodHappy) {
        if (nextMoodHappy) {
            document.getElementById("00").innerHTML = "<img src=\"/images/good.GIF\" width=\"300px\">";
            document.getElementById("03").innerHTML = "<img src=\"/images/good.GIF\" width=\"300px\">";
            document.getElementById("04").innerHTML = "<img src=\"/images/good.GIF\" width=\"300px\">";
            document.getElementById("05").innerHTML = "<img src=\"/images/good.GIF\" width=\"300px\">";
        } else {
            document.getElementById("00").innerHTML = "<img src=\"/images/goodtobad.GIF\" width=\"300px\">";
            document.getElementById("03").innerHTML = "<img src=\"/images/goodtobad.GIF\" width=\"300px\">";
            document.getElementById("04").innerHTML = "<img src=\"/images/goodtobad.GIF\" width=\"300px\">";
            document.getElementById("05").innerHTML = "<img src=\"/images/goodtobad.GIF\" width=\"300px\">";
        }
    } else {
        if (nextMoodHappy) {
            document.getElementById("00").innerHTML = "<img src=\"/images/badtogood.GIF\" width=\"300px\">";
            document.getElementById("03").innerHTML = "<img src=\"/images/badtogood.GIF\" width=\"300px\">";
            document.getElementById("04").innerHTML = "<img src=\"/images/badtogood.GIF\" width=\"300px\">";
            document.getElementById("05").innerHTML = "<img src=\"/images/badtogood.GIF\" width=\"300px\">";
        } else {
            document.getElementById("00").innerHTML = "<img src=\"/images/bad.GIF\" width=\"300px\">";
            document.getElementById("03").innerHTML = "<img src=\"/images/bad.GIF\" width=\"300px\">";
            document.getElementById("04").innerHTML = "<img src=\"/images/bad.GIF\" width=\"300px\">";
            document.getElementById("05").innerHTML = "<img src=\"/images/bad.GIF\" width=\"300px\">";
        }
    }
    currentMoodHappy = nextMoodHappy;
}

// Sensordaten und Votes anfordern (asynchron)
function GetData() {
    updateGif();
    var xmlHttpSd = new XMLHttpRequest();
    xmlHttpSd.onreadystatechange = function() {
        // empfangene Sensordaten vermerken
        if (xmlHttpSd.readyState == 4 && xmlHttpSd.status == 200) {

            var responseTextSd = xmlHttpSd.responseText;

            var doc = document.implementation.createHTMLDocument();
            doc.open();
            doc.write(xmlHttpSd.responseText);
            doc.close();

            var strSd = doc.getElementById("01").innerHTML;

            if (strSd == "empty" || strSd == "happy") {
                sdHappy = true;
            } else {
                sdHappy = false;
            }

            console.log("sdHappy:" + sdHappy);
        }

    }
    xmlHttpSd.open("GET", "http://192.168.178.48:8081/acclimate_mode/sensor-data", true); // true for asynchronous
    xmlHttpSd.send(null);

    var xmlHttpVote = new XMLHttpRequest();
    xmlHttpVote.onreadystatechange = function() {
        // empfangene Votes vermerken
        if (xmlHttpVote.readyState == 4 && xmlHttpVote.status == 200) {

            var responseText = xmlHttpVote.responseText;

            var doc2 = document.implementation.createHTMLDocument();
            doc2.open();
            doc2.write(xmlHttpVote.responseText);
            doc2.close();

            var strVote = "even";

            //if (responseText.localeCompare(str) == 0) {
            if (doc2.getElementById("02").innerHTML == strVote) {
                votesEven = true;
            } else {
                votesEven = false;
            }

            console.log("votesEven:" + votesEven);
        }

    }
    xmlHttpVote.open("GET", "http://192.168.178.48:8081/acclimate_mode/vote", true); // true for asynchronous
    xmlHttpVote.send(null);

    //setTimeout(updateGif(), 1000);

}
